# ASD_6_1 - Składowe spójne grafu

## Treść zadania

Dany jest nieskierowany graf prosty, zawierający nie więcej niż 100 wierzchołków, zdefiniowany przez listę jego krawędzi: krawędź łącząca wierzchołki i-ty oraz j-ty będzie opisana rozdzieloną spacjami parą liczb i oraz j. Z kolei, o liczności zbioru wierzchołków grafu można wnioskować na podstawie największej z liczb i lub j we wszystkich parach definiujących krawędzie.

Należy napisać program, który zidentyfikuje składowe spójne tego grafu.
### Format wejścia

Na wejściu programu, w kolejności jak niżej powinny się znaleźć następujące elementy:

Całkowita dodatnia liczba n określająca liczbę zestawów danych;
W każdym z n zestawów znajdują się:
Liczba całkowita dodatnia m określająca liczbę krawędzi grafu;
m par liczb całkowitych i oraz j definiujących kolejne krawędzieie grafu.

### Format wyjścia

Wyniki działania programu uzyskane dla kolejnych n zestawów danych zostaną na wyjściu programu oddzielone pustą linią.

Dla danego zestawu danych, jego składowe spójne zostaną podane w kolejnych liniach: najpierw pojawi się numer kolejnej składowej spójnej, a po dwukropku pojawią się odzielone spacjami kolejne wierzchołki grafu należące do tej składowej.
## Przykłady

### Przykład 1

#### Wejście

```
3
5
1 2
1 3
1 4
2 4
4 6
7
1 2
3 4
3 5
3 9
4 5
4 7
7 8
10
6 2
2 3
2 9
3 4
3 8
5 7
5 9
6 2
6 7
7 8
```

#### Wyjście

```
1: 1 2 3 4 6
2: 5

1: 1 2
2: 3 4 5 7 8 9
3: 6

1: 1
2: 2 3 4 5 6 7 8 9
```

## Opis rozwiązania

### Wczytywanie i przechowywanie danych

Na początku musimy wczytać graf i go reprezentować w pamięci. Do tego możemy użyć kodu wczytywania i listy sąsiedztwa z zadania [ASD_2_2](../ASD_2_2/writeup.md) trzeba jednak pamiętać o **kilku ważnych szczegółach":

1. Graf jest **nieskierowany**
    - Z tego wynika, **że każdej wczytanej krawędzi a->b musi odpowiadać krawędź b->a**. Jeśli używasz **mojej struktury z ASD_2_2** możesz to zrealizować następująco:
```c++
// Nazwy z przykładów z ASD_2_2, pierwsza wczytana liczba w linii przechowywana w zmiennej `a`, druga w `b`
lista_sas[a].push_back(b);
lista_sas[b].push_back(a);
```

2. Wierzchołki, **z których nie ma żadnej krawędzi, są ważne**. (ponieważ graf jest nieskierowany, to jeżeli nie ma krawędzi z (a więc i do) danego wierzchołka to będzie on sam stanowił składową spójną)
   - Jeśli używasz mojej implementacji z **ASD_2_2** możesz użyć następującej pętli, aby uzupełnić mapę **o puste listy** następująco:
```c++
// Nazwy z przykładów z ASD_2_2, zmienna `max` przechowuje numer największego wczytanego wierzchołka
// !! Przy założeniu, że numerujesz wierzchołki jak w zadaniu od 1, a nie od 0.
for (int i = 1; i <= max; i++) {
   if (lista_sas.find(i) == lista_sas.end()) {
        lista_sas[i] = list<int>();
   }
}
```

### Znajdowanie składowych spójnych

#### Wyjaśnienie intuicyjne

**Składowa spójna** to **fragment grafu**, którego **żaden wierzchołek nie jest połączony krawędzią z wierzchołkiem niewchodzącym w skład tej składowej**.

#### Przykład
Poniższy graf **ma 3 składowe spójne**, które zostały **oznaczone różnymi kolorami**.

![przykładowy graf](./example-graph.png)

#### Opis implementacji

Do tego najprościej będzie użyć algorytmu **DFS** (rekurencyjnego lub z jawnym stosem) lub **BFS**(gdzie potrzeba będzie kolejka np. zaimplementowana w [ASD_4_2](../ASD_4_2/writeup.md). Jest to kluczowy element tego algorytmu, więc nie wiem, czy rektor nie będzie miał uwag do użycia implementacji z biblioteki standardowej, dlatego zalecam użycie własnej z jednego z poprzednich zadań), które *jeśli dobrze pamiętam* **były w prezentacji z algorytmów grafowych z laboratorium**.

Musimy jednak dodać kilka elementów do wybranego algorytmu:
1. Tablica lub słownik gdzie **indeksy odpowiadają numerom wierzchołków** a wartości **numer składowej spójne, do której przynależą** (najprościej zrobić tablicę o rozmiarze o 1 większym i ignorować element zerowy) (wartości w związku z tym inicjalizujemy wartością symbolizującą brak składowej spójności (polecam `0`, ponieważ przy wypisywaniu indeksy podajemy, zaczynając od 1)
2. Algorytm wykonujemy **w pętli dla każdego wierzchołka, które nie przynależą do żadnej znalezionej do tej pory składowej spójnej**. W praktyce oznacza to pętlę odpowiadająca podobnemu pseudokodowi:
```python
num_obecnej_skladowej = 1
for i in range(1, max_wierzcholek + 1): # czyli 1...max_wierzchlek
    if nr_skladowej_spojnej[i] == 0:
        przejdz_graf(graf, i, nr_skladowej_spojnej, num_obecnej_skladowej) # Gdzie kolejne argumenty to
        num_obecnej_skladowej += 1
#1. Reprezentacja grafu
#2. Nr wierzchołka, od którego zaczynamy przechodzenie
#3. TABLICA reprezentująca, do której składowej spójnej należy który wierzchołek. Tablica ta JET MODYFIKOWANA w tej funkcji
#4. Numer obecnie oznaczanej składowej
```
3. Wybrany algorytm przechodzenia modyfikujemy tak, że w momencie **odwiedzania wierzchołka** `i` ustawiamy `nr_skladowej_spojnej[i]` na **numer obecnie poszukiwanej składowej spójnej**. *Szczegóły w pkt. 2*.

### Wypisywanie danych
Nie jestem pewien **jak bardzo elastyczny jest sposób sprawdzania** mogę tylko zagwarantować następujące rzeczy:
1. Akceptowane jest rozwiązanie, w którym **numery wierzchołków w składowej spójnej są posortowane rosnąco** a **składowe są posortowane w kolejności rosnącej względem najmniejszego wierzchołka**.
2. Pusta linia na końcu **całego** wyjścia oraz spacja na końcu linii są ignorowane.

Jeśli, używasz zasugerowanej implementacji na tablicy, możesz użyć **pętli podobnej do poniższego pseudokodu** aby spełnić wszystkie powyższe wymagania:
```python
for i in range(1, liczba_sklad_spojnych+1): # czyli 1...liczba_sklad_spojnych
    print(f"{i}: ", end="") # `end=""` aby nie dodawać nowej linii
    for j in range(1, max_wierzcholek + 1):
        if nr_skladowej_spojnej[j] == i:
            print(f"{j} ", end="") # `end=""` aby nie dodawać nowej linii
    print() # nowa linia
print() # pusta linia po każdym zestawie
```