# ASD_2_2 - Wycieczka

## Treść zadania

Mały Jasio żyje w przyszłości w wielkiej metropolii, gdzie można się poruszać od przystanku do przystanku pojazdami komunikacji miejskiej, zwanymi polibusami. Ze względu na różne zagrożenia cywilizacyjne (np. terroryzm, ptasia lub świńska grypa, choroba szalonych krów), komunikacja ta funkcjonuje w ten sposób, że po dojechaniu do dowolnego przystanku zawsze trzeba wysiadać z polibusu, który w tym czasie zostanie sprawdzony i zdezynfekowany przez odpowiednie służby miejskie. Jeżeli podróż powinna trwać dalej, to trzeba się przesiadać – nawet wtedy, gdy będziemy kontynuować dalszą drogę tym polibusem, z którego właśnie wysiedliśmy.

Mama Jasia wybiera się na zakupy, zabierając syna ze sobą. Ponieważ galerie handlowe znajdują się obok przystanków komunikacji miejskiej, dlatego przed wyruszeniem w trasę, mama robi listę przystanków, do których może się udać na zakupy, numerując je kolejnymi liczbami naturalnymi.

Ponieważ Jasio jest dużym chłopcem, dlatego mama będąc w sklepie czasami pozwala mu w tym czasie robić wycieczki po mieście do tych przystanków, które znajdują się na jej liście. Oczywiście Jasio mówi mamie, dokąd zamierza dotrzeć. Po zakupach mama Jasia jedzie po niego w umówione miejsce.

Jasio jest oryginałem, dlatego w trakcie każdej z wycieczek dokładnie jeden raz się przesiada. Tymczasem mama nie znosi się przesiadać, dlatego zgodę na wycieczkę daje tylko wtedy, gdy ma pewność, że w drodze po syna nie będzie się przesiadać. Jeszcze przed wyruszeniem w drogę, nie wiedząc, dokąd uda się na zakupy, mama Jasia musi podjąć decyzję, czy ewentualnie może zgodzić się na wycieczkę syna. Dopiero po tej decyzji następuje wybór miejsca zakupów.

Ponieważ nie jest łatwo zdecydować, czy Jasio będzie mógł udać się na wycieczkę, jego mama liczy na Twoją pomoc. W tym celu musisz napisać odpowiedni program, który pomoże tę decyzję podjąć.

Potraktuj listę przystanków wybranych przez mamę Jasia, jako węzły grafu. Z kolei, możliwe połączenia pomiędzy nimi potraktuj, jako łuki tego grafu. Powyższy graf będziemy dalej nazywać grafem MJ (graf Mamy Jasia).

Napisz program, który dla każdego zestawu danych będzie sprawdzał czy w grafie MJ, każdej marszrucie zawierającej jedną przesiadką (potencjalnej marszrucie Jasia) odpowiada marszruta nie zawierająca żadnej przesiadki (potencjalna marszruta Mamy Jasia).

### Format wejścia

Potencjalne marszruty Mamy po to, aby Jasia odebrać z wycieczki są zdefiniowane w postaci pary dwóch liczb i oraz j (1 ≤ i, j ≤ 100). Jeżeli z i-tego przystanku jest bezpośrednie połączenie z przystankiem j-tym, to na wejściu w osobnym wierszu pojawi się rozdzielona spacjami para liczb i oraz j. Liczba wierszy w zestawie danych jest równa liczbie par (i,j), dla których istnieje połączenie bezpośrednie (bez konieczności przesiadki), czyli marszruta o długości jednego łuku.

O wielkości zbioru możliwych do odwiedzenia przystanków można wnioskować na podstawie największej z liczb i lub j w parach.

### Format wyjścia

Na wyjściu programu należy podać odpowiedź TAK albo NIE. Będzie to odpowiedź na pytanie, czy Mama może wyrazić zgodę na wycieczkę Jasia.

W języku teorii grafów będzie to odpowiedź na pytanie, czy w grafie MJ, każdej marszrucie zawierającej jedną przesiadką odpowiada marszruta nie zawierająca żadnej przesiadki.

## Przykłady

### Przykład 1

#### Wejście

```
2 3
2 9
3 4
5 7
5 9
6 7
7 8
8 3
```

#### Wyjście

```
NIE
```

### Przykład 2

#### Wejście

```
1 1
2 1
2 2
2 4
3 1
4 1
4 2
4 4
```

#### Wyjście

```
TAK
```

## Opis rozwiązania

Pytanie w tym zadaniu jest efektywnie "**czy każdej ścieżce o długości 1 odpowiada ścieżka od długości 2?**".
Najprostszym sposobem rozwiązania tego problemu będzie **skorzystanie z list sąsiedztwa**

### List sąsiedztwa

Jest to **struktura, w której każdemu wierzchołkowi przypisana jest lista wierzchołków, do których można z niego projeść**.
Dla **przykładu** grafowi:

![img.png](example-graph.png)

**odpowiada** lista sąsiedztwa:

| Wierzchołek | Wierzchołki, do których jest krawędź |
|-------------|--------------------------------------|
| 1           | 2,3,4                                |
| 2           | 1,4                                  |
| 3           | 2                                    |
| 4           | 3                                    |

Drugą kolumnę tabeli **reprezentujemy najczęściej przy pomocy list (Linked list).**

### Uwaga do podanego dalej rozwiązania

Podane dalej rozwiązanie jest **proste, dość standardowe i niekoniecznie optymalne** (zbliżona do implementacji dr. Figielskiej). Jak ktoś by był zainteresowany krótszym i bardziej wydajnym rozwiązaniem dla tego zadania jest rozwiązanie oparte na **mapach (słownikach) i zbiorach** (natomiast tu musicie wymyślić sami :D).

### Implementacja listy sąsiedztwa w C++

Przy użyciu następujących nagłówków:
- `<list>`
- `<unordered_map>`

#### Operacje na takiej liście sąsiedztwa

##### Tworzenie list sąsiedztwa

```c++
unordered_map<int, list<int>> lista_sas;
```

##### Dodawanie krawędzi

```c++
lista_sas[z_kad].push_back(do_kad);
```

Dla przykładu krawędź z `1` do `2` dodajemy w następujący sposób:
```c++
lista_sas[1].push_back(2);
```

##### Sprawdzanie, czy krawędź z `z_kad` do `do_kad` istnieje

###### Jeśli nie używamy nagłówka `<algorithm>`
```c++
for (auto cel : lista_sas[z_kad]) {
    if (cel == do_kad) {
        return true;
    }
}
return false;
```

###### Jeśli używamy nagłówka `<algorithm>`
```c++
find(lista_sas[z_kad].begin(), lista_sas[z_kad].end(), do_kad) != lista_sas[z_kad].end(); // zwraca true, 
// jeśli sprawdzany wierzchołek znajduje się w liście sąsiedztwa.
```

##### Iteracja po wszystkich wierzchołkach, z których wychodzą krawędzie

```c++
for (const auto & krawedz : lista_sas) {
    int z_kad = krawedz.first;
    // ... tu piszemy kod używający wierzchołków
}
```

##### Iteracja po wszystkich krawędziach

```c++
for (const auto & krawedz : lista_sas) {
    int z_kad = krawedz.first;
    for (auto do_kad : krawedz.second) {
        // z_kad - wierzchołek, z którego jest krawędź
        // do_kad - wierzchołek, do którego jest krawędź
    }
}
```

##### Iteracja po wszystkich wierzchołkach, do których jest krawędź z danego wierzchołka
```c++
for (auto do_kad : lista_sas[z_kad]) {
        // do_kad - wierzchołek, do którego jest krawędź
}
```

### Rozwiązanie zadania przy użyciu wyżej opisanej struktury

Mamy sprawdzić, czy

>**dla każdej krawędzi A->B**
> 
>istnieje
> 
>**droga o długości 2 z A do B**


Jak **iterować po krawędziach** pokazano powyżej, natomiast sprawdzenie, czy istnieje droga o długości 2 pomiędzy wierzchołkami A i B to efektywnie odpowiedź na pytanie:
> Czy, z **któregokolwiek** wierzchołka, do którego **jest krawędź z A** istnieje **krawędź do B.**

Wiedząc to, można napisać **pseudokod rozwiązania** (bazowany na języku python):

```python
for z_kad, do_kad in krawedzie:
    czy_jakikolwiek = False
    for posredni in lista_sas[z_kad]:
        if (posredni, do_kad) in krawedzie:
            czy_jakikolwiek = True
            break
    if not czy_jakikolwiek:
        print("NIE")
        exit() # Kończy działanie programu
print("TAK")
```