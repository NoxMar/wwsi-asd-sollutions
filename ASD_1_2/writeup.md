# ASD_1_2 - Minimalna liczba w ciągu

## Treść zadania

Dany jest ciąg liczb całkowitych zakończony zerem, przy czym zero nie jest elementem tego ciągu. Liczby w ciągu mogą się powtarzać.

Należy znaleźć minimalną liczbę w ciągu oraz jej ostatnie położenie.

### Format wejścia

Na wejściu znajduje się ciąg liczb całkowitych, z przedziału `[-100;100]`, zakończony zerem. Zero służy do określenia końca ciągu, w związku z czym wystąpi na wejściu dokładnie raz. Każdy element ciągu podany jest w osobnej linii.

### Format wyjścia
Na wyjściu należy w osobnych liniach wypisać dwie liczby całkowite wartość najmniejszej liczby w danym ciągu oraz jej ostatnie położenie.

## Przykłady

### Przykład 1

#### Wejście

```
4
3
7
-2
3
7
-2
11
4
0
```

#### Wyjście

```
-2
7
```

## Opis rozwiązania

Ponieważ rozwiązania jest **bardzo proste** umieszczę tylko **schemat blokowy rozwiązania** oraz krótkie porównanie z **zadaniem [ASD_1_1](../a1/writeup.md)**.

### Porównanie z [ASD_1_1](../a1/writeup.md)

- Szukamy liczby **NAJMNIEJSZEJ** a **NIE największej** więc zmienną do przechowywania obecnie najmniejszej wartości musimy zainicjalizować na liczbę **większa lub równą maksymalnej wartości z zakresu**. W tym przypadku zakres jest do **100 włącznie**, więc wybieram wartość **101**.
- Szukamy **ostatniego** wystąpienia najmniejszej liczby, więc musimy **aktualizować indeks również, jeśli znajdziemy liczbę równą najmniejszej do tej pory**. Z tego powodu powinniśmy użyć porównania *mniejsze **lub równe*** - **\<\=** 
- Liczby w ciągu są numerowane **od 1**.

### Schemat blokowy rozwiązania

```mermaid
flowchart TD
    %%flow to exit
    start(Start)
    
    define-vars[currentMin = 101<br />lastMinIndex = 0<br />currentIndex = 0]
    start --> define-vars
    

    read-val[/wczytaj nextVal/]
    define-vars --> read-val
    read-val --> increment-index
    increment-index[currentIndex += 1]
    
    if-zero{nextVal==0}
    increment-index --> if-zero
    
    print-current-min[/wypisz currentMin<br />wypisz lastIndex/]
    if-zero --> |TAK| print-current-min
    
    exit(Exit)
    print-current-min --> exit
    
    
    %% Flow through potentiol update
    if-next-val{nextVal <= currentMin}
    if-zero --> |NIE| if-next-val
    
    
    %% Update currentMax if nextVal is greater
    update-max[currentMin=nextVal<br />lastIndex=currentIndex]
    if-next-val --> |TAK| update-max
    if-next-val --> |NIE| read-val
    %% Loop back to read
    update-max --> read-val
```
