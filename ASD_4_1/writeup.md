# ASD_4_1 - Mnożenie transpozycji macierzy przez wektor

## Treść zadania

Dana jest tabela zawierająca liczby rzeczywiste. Tabela składa się z m wierszy oraz n+1 kolumn. Kolejne n kolumn tabeli stanowi reprezentację pewnej macierzy X , zaś kolumna n n+1 -sza jest reprezentacją wektora y .

Napisać program, który znajdzie iloczyn XTy.

### Format wejścia

Na wejściu programu znajdują się następujące elementy:

n - liczba naturalna 1 ≤ n ≤ 25 , określająca ilość kolumn macierzy X,
m - liczba naturalna 5 ≤ m ≤ 1000 , określająca liczbę wierszy z danymi,
tabela o rozmiarze m × (n+1) zawierająca oddzielone spacjami liczby rzeczywiste:
x11 x12 … x1n y1
x11 x12 … x1n y2
… … … … …
xm1 xm2 … xmn ym

### Format wyjścia

Na wyjściu programu powinien się znaleźć wynik mnożenia transpozycji danej macierzy X przez dany wektor y.

## Przykłady

### Przykład 1

#### Wejście

```
1
15
4.46 36.01
5.36 46.08
4.27 36.61
5.33 43.62
3.80 29.17
5.32 47.97
3.98 32.74
5.24 45.00
3.37 27.55
5.21 46.63
4.50 36.91
4.89 42.10
4.72 40.06
4.62 37.30
4.57 40.50
```

#### Wyjście

```
2782.8083
```

### Przykład 2

#### Wejście

```
3
9
1 1.5 0.5 -0.55
1.5 1 1 2.7
2 -0.5 1.5 7.45
2.5 0 0 3.2
-3 0.5 -0.5 -10.05
3.5 -1 -1 3.7
4 1.5 -1.5 -0.55
4.5 2 0 4.2
5 -2.5 0.5 13.45
```

#### Wyjście

```
153.45 -36.625 22.475
```

## Opis rozwiązania

To zadanie jest prostszą wersją zadania [ASD_1_4 - Iloczyn macierzy](../ASD_1_4/writeup.md).

Ponieważ mnożymy tu **macierz transponowaną przez wektor** zadanie można przepisać jako:

Wypisz **sumę iloczynów elementów kolumny tablicy** 2-wymiarowej raz **odpowiadające** im indeksami **elementy tablicy jednowymiarowej**.
**Powtórz dla każdej kolumny**.

### Uwagi wstępne

Ponieważ wyniki sprawdzane są do **6 miejsca po przecinku** musimy **skonfigurować strumień wyjściowy** aby wypisywał liczby w odpowiednim formacie

Mamy tu do dyspozycji 2 opcje:

#### Format liczb zmiennoprzecinkowych ze stałą liczbą miejsc po przecinku

W dowolnym momencie kodu przed wypisywaniem wyników należy użyć następujących linii

*Zakłada użycie nagłówka `<iostream>` i instrukcji `using namespace std;`*

```c++
cout.precision(6); // ustawia maksymalną precyzję wyświetlania liczb zmiennoprzecinkowych na 6
cout << fixed; // ustawia tryb wyświetlania liczb zmiennoprzecinkowy na taki, w którym zawsze wyświetlana jest liczba
// miejsc po przecinku ustawiona w `precision`.
```

#### Użycie maksymalnej dostępnej dla typu precyzji jednak bez uzupełniana zerami

W dowolnym momencie kodu przed wypisywaniem wyników należy użyć następujących linii

*Zakłada użycie nagłówka `<iostream>`, `<limits>` i instrukcji `using namespace std;`*

```c++
cout.precision(std::numeric_limits<double>::digits10); // Ustawia precyzję na maksymalną liczbę cyfr znaczących typu 
// (w tym przypadku `double`).
```

#### Przechowywanie danych 

Zakładając użycie jakiegokolwiek współczesnego standardu c++ (na SPOJ **gcc C++ 14**) po **wczytaniu**:
- **pierwszej** linii wejścia - `n_kolumn`
- **drugiej** linii wejścia - `n_wierszy`

```c++
double macierz[n_wierszy][n_kolumn];
```

To **samo** można zrobić dla **wektora wyjściowego** oraz (jeśli ktoś zdecyduje się używać, **co jest niekonieczne**).

#### Fragment kodu rozwiązania w języku python3

Ten fragment dokonuje tylko **obliczeń i wypisywania wyników**.

Kod zakłada, że następujące zmienne zostały wczytane od użytkownika zgodnie z treścią zadania.

- `macierz` - macierz, której transpozycję mnożymy
- `wektor` - wektor, przez który mnożymy
- `n_kolumn` - liczba kolumn mnożonej macierzy
- `n_wierszy` - liczna wierszy mnożonej macierzy (i elementów wektora)
 
```python
def oblicz(n_kolumn, n_wierszy, macierz, wektor):  # definicja funkcji, aby usunąć błędy sprawdzania 
    # fragmentów kodu w markdown
    for i in range(n_kolumn):
        wynik = 0.0
        for j in range(n_wierszy):
            wynik += macierz[j][i] * wektor[j]
        print(round(wynik, 6), end=" ")  # round zaokrągla liczbę w tym przypadku do 6 miejsc po przecinku

    print()  # wypisuje nową linię
```