# ASD_1_3 - Najmniejsza i największa liczba w ciągu

## Treść zadania

Dany jest ciąg liczb całkowitych. Liczby w ciągu mogą się powtarzać.

Należy znaleźć najmniejszą i największą liczbę w ciągu. Dla liczby najmniejszej należy podać jej najwcześniejsze położenie w ciągu, zaś dla liczby największej, jej ostatnie położenie w ciągu.

### Format wejścia

Na wejściu znajduje się znajduje się ciąg liczb całkowitych z przedziału [-500;500]. Każdy element ciągu podany jest w osobnej linii.

### Format wyjścia

Na wyjściu należy wypisać w osobnych liniach cztery liczby całkowite oznaczające kolejno:

1. Wartość najmniejszej liczby w danym ciągu
2. Najwcześniejsze położenie liczby najmniejszej
3. Wartość największej liczby w danym ciągu
4. Ostatnie położenie liczby największej

## Przykłady

### Przykład 1

#### Wejście

```
4
-3
7
2
-1
11
-3
7
11
2
4
```

#### Wyjście

```
-3
2
11
9
```

## Opis rozwiązania

Ponieważ rozwiązania jest **bardzo proste** umieszczę tylko **schemat blokowy rozwiązania**. Jest ono połączeniem lekko zmodyfikowanych zadań **ASD-1-1** i **ASD_1_2**. Jedynym, **co może sprawić trudność**, jest **wczytywanie danych** (to pierwsze zadanie z tego typu formatem wejścia), dlatego załączam [**instrukcję wczytywania takich danych**](../docs/reading-data.md#nieznana-liczba-danych-wejciowych-brak-symbolu-koczcego).

**UWAGA**: Warto zwrócić uwagę, że ponieważ interesuje nas **pierwszy** indeks dla wartości minimalnej używamy porównania **MNIEJSZE**(`<`). Analogicznie, ponieważ interesuje nas **ostatni** indkes elementu największego używamy porównania **WIĘKSZE LUB RÓWNE**(`>=`).

```mermaid
graph TD
    start(Start)
    
    define-vars[currentMin=501<br />firstMinIndex=0<br />currentMax = -501 <br /> lastMaxIndex = 0 <br /> index = 0]
    
    read-next-elem[/wczytaj nextVal/]
    if-should-continue{Czy podano poprawne dane?}
    
    increment-index[index += 1]
    
    check-lower{nextVal < currentMin}
    update-lower[currentMin = nextVal <br /> firstMinIndex = index]
    
    check-higher{nextVal >= currentMax}
    update-higher[currentMax = nextVal<br />lastMaxInedx = index]
    
    
    write-results[wypisz currentMin<br />wypisz firstMinIndex<br />wpisz currentMax<br />wypisz lastMaxIndex]
    exit(Zakoncz)
    
    
    start --> define-vars
    define-vars --> read-next-elem
    
    read-next-elem --> if-should-continue
    if-should-continue --> |NIE| write-results
    if-should-continue --> |TAK| increment-index
    
    increment-index --> check-lower
    

    check-lower --> |TAK| update-lower
    check-lower --> |NIE| check-higher
    update-lower --> check-higher
    
    
    check-higher --> |TAK| update-higher
    check-higher --> |NIE| read-next-elem
    update-higher --> read-next-elem
    
    write-results --> exit
```