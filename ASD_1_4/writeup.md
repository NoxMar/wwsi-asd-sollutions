# ASD_1_4 - Iloczyn macierzy

## Treść zadania

Dana jest prostokątna macierz X, zawierająca liczby całkowite, składająca się z m wierszy i n kolumn. Liczba wierszy i kolumn jest nie większa niż 100.

Należy napisać program, który znajdzie iloczyn macierzy $X^TX$.

### Format wejścia

W pierwszej linii wejścia znajduje się liczba całkowita m ∈ [1;100] określająca liczbę wierszy macierzy X. W drugiej linii wejścia znajduje się liczba całkowita n ∈ [1;100] określająca liczbę kolumn macierzy X. W kolejnych m liniach znajduje się po n liczb całkowitych, z przedziału [-100;100], oddzielonych spacją określających elementy macierzy X.

### Format wyjścia

Na wyjściu należy wypisać macierz wynikową $X^TX$.

## Przykłady

### Przykład 1

#### Wejście

```
5
3
1 0 1
3 0 4
2 1 -1
4 3 2
-4 3 3
```

#### Wyjście

```
46 2 7 
2 19 14 
7 14 31
```

## Opis rozwiązania

### Ogólny wstęp

Mnożenie macierzy jest dość prostym zadaniem (jeśli rozmiar macierzy nie jest za duży). Dla przypomnienia:
1) Mnożenie macierzy **NIE JEST PRZEMIENNE** 
2) W mnożeniu $A*B$ macierz **A musi mieć tyle samo kolumn co B wierszy**.
3) Element o współrzędnych `i`(kolumna), `j`(wiersz) $A*B$ jest **sumą iloczynów kolejnych wyrazów i-tego wiersza i j-tej kolumny**.

Więc w przypadku ogólnym najprościej rozwiązać je **trzykrotnie zagnieżdżoną pętlą for**. W przypadku ogólnym (pseudokod):
*A i B są macierzami, A ma tyle samo kolumn co B wierszy*:
```python
for i in range(len(A)): # i dla każdego numera wiersza macierzy A
    for j in range(len(B[0])): # j dla każdego numeru kolumny macierzy B
        wynik[i][j] = 0 # wynik w wierszu i kolumnie j
        for k in range(len(B)): # dla każdego numeru k kolumny A/wiersza B (aby, spełniać warunek mnożenia macierzy A musi mieć tyle samo kolumn co B wierszy)
            wynik[i][j] += A[i][k] * B[k][j]
```
#### Zapis bardziej matematycznych
```math
AB_{ij} = \sum_{k=1}^n A_{ik}B_{kj}
```
Gdzie:
- **i** to wiersz wyniku
- **j** to kolumna wyniku
- **n** to liczba kolumn A/ wierszy B (muszą być sobie równe)

**Macierz transponowana** to macierz, "**obrócona o 90 stopni w prawo**" tak, że **wiersze stają się kolumnami (o tym samym numerze)**. Dzięki temu **warunek 2 jest ZAWSZE ZASPOKOJONY**.

Z tego powodu mnożenie macierzy przez jej transpozycję ($A*A^T$) można zapisać pętlą:
```python
for i in range(len(A)): # i dla każdego numera wiersza macierzy A
    for j in range(len(A)): # j dla każdego numeru kolumny A^T (czyli wiersza macierzy A)
        wynik[i][j] = 0 # wynik w wierszu i kolumnie j
        for k in range(len(A[0])): # dla każdego numeru k kolumny A/wiersza A^T
            wynik[i][j] += A[i][k] * A[j][k]
```
#### Zapis bardziej matematycznych
```math
(A*A^T)_{ij} = \sum_{k=1}^n A_{ik}A_{jk}
```
Gdzie:
- **i** to wiersz wyniku
- **j** to kolumna wyniku
- **n** to liczba kolumn A
- 
### Wynikowy kod mnożenia macierzy w python
Jednak **ponieważ mnożymy $A^T*A$** a nie $A^T*A$** powoduje to **odwrócenie indeksów w naszych pętlach**:
```python
for i in range(len(A[0])): # i dla każdego numera wiersza macierzy A^T (kolumny macierzy A)
    for j in range(len(A[0])): # j dla każdego numeru kolumny macierzy A (wiersza A^T)
        wynik[i][j] = 0 # wynik w wierszu i kolumnie j
        for k in range(len(A)): # dla każdego numery k kolumny A^T/wiersza A
            wynik[i][j] += A[k][i] * A[k][j]
```

#### Zapis bardziej matematycznych
```math
(A^T*A)_{ij} = \sum_{k=1}^n A_{ki}A_{kj}
```
Gdzie:
- **i** to wiersz wyniku
- **j** to kolumna wyniku
- **n** to liczba wierszy A

### Uwagi implementacyjne dla języka C++

Ponieważ **nie znamy rozmiaru macierzy A** przed uruchomieniem programu potrzebujemy **dynamicznie alokować tablicę dwuwymiarową** (lub stworzyć tablicę 100 x 100 ale to trochę prymitywne, choć prawdopodobnie szybsze).
Można to wykonać następującym kodem:
```c++
int liczba_wierszy, liczba_kolumn;
cin >> liczba_wierszy >> liczba_kolumn;
int** macierz = new int* [liczba_wierszy];
for (int i = 0; i < liczba_wierszy; i++) {
    macierz[i] = new int[liczba_kolumn];
}
```
W wyniku jego działania mamy tablicę 2 wymiarową reprezentującą macierz `liczba_wierszy X liczba_kolumn` (`wiersze X kolumny`) gdzie dostęp do elementu w **i-tym wierszu j-tej kolumnie mamy przez `macierz[i][j]`**.

W tym przypadku nie ma potrzeby, jednak jeśli kod miałby **działać dłużej**, niż jest **potrzebną macierz** należy **zwolnić** zajmowaną przez nią **pamięć**. Co ważne **NAJPIERW** musimy usunąć wskaźniki w tablicy (wskaźników na tablice `int`) **`delete[] macierz[i]` dla każdego wiersza** a na koniec `delete[] macierz` aby usunąć **tablicę** (wskaźników na tablice typu `int`).

Wczytywanie danych jest w tym przypadku dość trywialne, więc nie ma co go opisywać.

#### Obliczenia i wypisywanie danych

Ponieważ **nie** ma sensu **zapisywać wyników w pamięci** dużo prościej będzie **wypisywać je na bieżąco**. Najprościej to osiągnąć w następujący sposób (ominięto fragmenty odpowiedzialne za obliczania, aby kod nie był zbyt podobny w różnych implementacjach)

```c++
for (int i = 0;/*pętla po wierszach macierzy A^T (kolumnach A)*/) {
    for(int j = 0;/*pętla po kolumnach A^T (wierszach A))*/) {
        int wynik = 0; //Wynik dla wiersza i kolumny j
        // ... Obliczanie wyniku... (tak łatwo nie ma :D)
        cout << wynik;
        if (liczba_kolumn - j > 1) { // Jeśli to nie jest ostatnia liczba w wierszu to wypisz po niej spację.
            cout << " ";
        }
    }
    cout << endl; // Wypisz nową linię na koniec każdego wiersza
}
```
#### Uwagi końcowe
- **liczba kolumn** macierzy **transponowanej** jest równa **liczbie wierszy oryginalnej** macierzy (i vice versa). Co za tym idzie, jeśli są **zapisane w tej samej tablicy** i iterujemy po **wierszach A^T** i **kolumnach A** w obu przypadkach **iterujemy do `liczba_kolumn`**.
- Opisane rozwiązanie jest **optymalizowane pod kątem pamięci** (nie przechowujemy całej macierzy wynikowej). Jeśli potrzeby jest nam **później wynik** a jednocześnie **chcemy oszczędzić pamięć** (lub optymalizujemy pod kątem złożoności obliczeniowej) prosto zauważyć, że macierz wynikowa **jest symetryczna** więc wystarczy przechować w pamięci wartości **pod i na przekątnej**. Podejście takie pozwala przechować w **pamięci $\frac{n^2+n}{2}$ zamiast $n^2$ elementów**. Dodatkowo **redukuje to ilość iteracji** pętli wewnętrznej (*pętla wewnętrzna zawsze ma `liczba_wierszy` iteracji*) **z $n^2$ do $\frac{n^2+n}{2}$** (gdzie n jest równe `liczba_kolumn`).
    
    **Porównanie:**

    ![Wykres porównujący złożoność obliczeniową i pamięciową obu wariantów](plot_complexity_matrix_symetric.png "Wykres porównujący złożoność obliczeniową i pamięciową obu wariantów")