# ASD_5_1 - Sortowanie

## Treść zadania

Napisać program, który będzie sortował ciąg liczb całkowitych. Długość ciągu jest nieznana. Wiadomo jedynie, że liczba elementów w ciągu nie będzie większa niż 100000 elementów.

### Format wejścia

Na wejściu znajdzie się pewna, nieznana ilość liczb całkowitych.

### Format wyjścia

Na wyjściu znajdą się wszystkie liczby posortowane niemalejąco.

## Przykłady

### Przykład 1

#### Wejście

```
2
4
-9
-3
-4
0
7
4
3
-2
-5
6
-3
7
-6
```

#### Wyjście

```
-9
-6
-5
-4
-3
-3
-2
0
2
3
4
4
6
7
7
```

## Opis rozwiązania

Zadanie to polega na zaimplementowaniu dowolnego sortowania dla tablicy o nieznanym wcześniej rozmiarze. Samego algorytmu sortowania **nie mogę tu podać**, ponieważ algorytmów tych jest **sporo** i jak wszyscy **zaimplementują to samo** (w ten sam sposób), to będzie to **podejrzane**.

Wspomnę tylko, że algorytm prawdopodobnie powinien mieć **złożoność najgorszego przypadku `O(n long n)`** (lub lepszą). Takie algorytmy to np.:
- Merge sort (sortowanie przez scalanie)
- Heap sort (sortowanie przez kopcowanie)
- Quick Sort (sortowanie szybkie)
- Timsort 

**Niektóre** (quicksort i chyb mergesort) z tych algorytmów **implementowaliśmy na laboratoriach z AiSD** więc można **po prostu skopiować tę implementację ze swojego kodu** (lub użyć pseudokodu z prezentacji do laboratoriów jako bazy implementacji).

### Wczytywanie danych

Do wczytywania danych możemy **użyć jednej z 2 metod**:

#### Wczytywanie do tablicy statycznej

Wiemy, że wejście może zawierać **maksymalnie 100000 elementów** więc możemy zadeklarować tablicę tej wielkości i zliczać wczytywane elementy. Przykład kodu:

**POLECAM TROCHĘ ZMIENIĆ NAZWY KOLEJNOŚĆ ITP. :D**

```c++
#include <iostream>

#define MAX_ARRAY_SIZE 100000

using namespace std;

int main() {
    int dane[MAX_ARRAY_SIZE];
    int liczba_danych = 0;
    int x;
    
    while ( cin>>x ) {
        dane[liczba_danych] = x;
        liczba_danych++;
    }
    
    // W tym momencie mamy tablicę dane, w której na miejscach 0...`liczba_danych` znajdują się wczytane wartości.
    // Dalej można implementować sortowanie.
}
```