# FIBONUMS — Liczby Fibonacciego

## Treść zadania

Mając daną liczbę N oblicz N-tą liczbę fibonacciego modulo 1000000007 = 10^9 +7

### Format wejścia

W pierwszym wierszu liczba testów T<=100.

W następnych T liniach liczby Ni

Ni<=10^9

### Format wyjścia

Dla każdego testu w osobnej linii Ni-ta liczba ciągu fibonacciego modulo 10^9+7

## Przykłady

### Przykład 1

#### Wejście

```
2
10
10000
```

#### Wyjście

```
55
271496360
```

## Opis rozwiązania

### Metoda fast doubling

#### Podstawy matematyczne

Dla ciągu Fibonacciego mamy 2 następujące zależności:

```math
F(2n) = F(n)(2F(n+1)-F(n)) \\
F(2n+1) = F(n)^2 + F(n+1)^2
```
Gdzie **F** oznacza **funkcję Fibonacciego**.

(Dla ciekawych wynikają one z jawnego wyciągnięcia tylko użytecznego wektora z zależności:
```math
\begin{bmatrix}
1 & 1 \\
0 & 1
\end{bmatrix}
=
\begin{bmatrix}
F_{n+1} & F_n \\
F_n & F_{n-1}
\end{bmatrix}
```
)

Biorąc to pod uwagę możemy szybko obliczyć `F(n)` jeśli mamy `F(n/2)` dla liczb niepatrzystych. Alternatywnie dla liczby nieparzystej `n+1` możemy obliczyć `F(n+1)`znając `F(n/2)`.

Biorąc to pod uwagę możemy prosto stworzyć procedurę rekurencyjną, której warunkiem stopu są znane z definicji wyrazy ciągu Fibonacciego:

| n   | F(n) |
|-----|------|
| 0   | 1    |
| 1   | 1    |

#### Kod w python
Ponieważ w przypadku **tego algorytmu** kod w **python** będzie bardzo zbliżony do **czytelnego pseudokodu** użyję po prostu kodu w tym języku.
Ten kod jest **implementacją całego algorytmu jako funkcji i **nie jest wykonywalna**. Pełna wersja programu w języku python znajduje się [**TUTAJ**](./fibonums.py)

```python
MOD = 1000000007
# result_vector zawiera w sobie 2 elementy, po kolei [F(2n), F(2n+1)]
def fast_doubling_fibo(n, result_vector):
    if n == 0:
        result_vector[0] = 0
        result_vector[1] = 1
        return
    fast_doubling_fibo(n//2, result_vector) # // to python dzielenie całkowitoliczbowe to samo co np. w C++ / dla argumentów typu całkowitoliczbowego
    
    f_n = result_vector[0]
    f_n_plus_1 = result_vector[1]
    
    f_2n = 2*f_n_plus_1 - f_n
    # Ponieważ liczby mogą być duże i istnieje na tym etapie ryzyko owerflow, musimy sprawdzić, czy nie ma potrzeby skorygowania wyniku
    if f_2n < 0:
        f_2n += MOD
    f_2n = (f_2n * f_n) % MOD
    
    f_2n_plus_1 = (f_n_plus_1 * f_n_plus_1 + f_n * f_n) % MOD
    
    # Jeśli n było nieparzyste wynik dzielenia całkowitoliczbowego przez 2 był niższy niż n/2 ( (2k+1)/2 = k +0.5 vs (2k+1)//2 = k)
    if n % 2 == 1:
        # Jeśli tak to jako f_2n przyjmujemy f_2n_plus_1 a f_2n_plus_1 wyliczamy z definicji fibo.
        result_vector[0] = f_2n_plus_1
        result_vector[1] = (f_2n_plus_1 + f_2n) % MOD
    else:
        result_vector[0] = f_2n
        result_vector[1] =  f_2n_plus_1
    return
```