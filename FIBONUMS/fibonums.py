#!/usr/bin/env python3

MOD = 1000000007


# result_vector zawiera w sobie 2 elementy, po kolei [F(2n), F(2n+1)]
def fast_doubling_fibo(n, result_vector):
    if n == 0:
        result_vector[0] = 0
        result_vector[1] = 1
        return

    # // to python dzielenie całkowitoliczbowe to samo co np. w C++ / dla argumentów typu całkowitoliczbowego
    fast_doubling_fibo(n//2, result_vector)

    f_n = result_vector[0]
    f_n_plus_1 = result_vector[1]

    f_2n = 2*f_n_plus_1 - f_n
    # Ponieważ liczby mogą być duże i istnieje na tym etapie ryzyko owerflow, musimy sprawdzić,
    # czy nie ma potrzeby skorygowania wyniku
    if f_2n < 0:
        f_2n += MOD
    f_2n = (f_2n * f_n) % MOD

    f_2n_plus_1 = (f_n_plus_1 * f_n_plus_1 + f_n * f_n) % MOD

    # Jeśli n było nieparzyste wynik dzielenia całkowitoliczbowego przez 2 był niższy niż n/2
    # ( (2k+1)/2 = k +0.5 vs (2k+1)//2 = k)
    if n % 2 == 1:
        # Jeśli tak to jako f_2n przyjmujemy f_2n_plus_1 a f_2n_plus_1 wyliczamy z definicji fibo.
        result_vector[0] = f_2n_plus_1
        result_vector[1] = (f_2n_plus_1 + f_2n) % MOD
    else:
        result_vector[0] = f_2n
        result_vector[1] = f_2n_plus_1
    return


# KONIEC ALGORYTMU TO TYLKO DRIVER CODE dla wejścia w postaci podawanej przez SPOJ.
count = int(input())
results = [0, 0]
for i in range(count):
    n = int(input())
    fast_doubling_fibo(n, results)
    print(results[0])
