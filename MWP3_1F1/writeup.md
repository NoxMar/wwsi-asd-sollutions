# MWP3_1F1 - Relacja

## Treść zadania

Gimnazjalni koledzy Jasia postanowili zagrać w grę planszową, która składa się z wielu etapów. W każdym z etapów odbywa się rozgrywka, po zakończeniu której, aby przejść na wyższy poziom trzeba rozwiązać pewne dziwne zadanie matematyczne. Jaś chętnie by wziął udział w grze, ale nie bardzo potrafi rozwiązać to zadanie, gdyż nie uważał na lekcjach matematyki. Potrzebuje pomocy. Pomóż mu, pisząc odpowiedni program.

Treścią zadania jest odpowiedź na pytanie, jakie własności i jaki typ ma pewna relacja dwuargumentowa określona w zbiorze kolejnych liczb całkowitych dodatnich, składającym się z nie więcej niż stu elementów. O wielkości zbioru, w którym określona jest ta relacja można wnioskować na podstawie największej z liczb i lub j w parach. Za każdym razem relacja jest zdefiniowana w następujący sposób: jeżeli i-ty element jest w relacji z elementem j-tym, to na wejściu w osobnym wierszu pojawi się rozdzielona spacjami para liczb i oraz j. Liczba wierszy w zestawie danych jest równa liczbie par (i,j) dla których zachodzi relacja.

### Format wejścia

Linijki zawierające 2 linie. Każda linia oznacza `i j` oznacza *i **jest w relacji** z j*.

### Format wyjścia

Wynikiem działania programu będą, co najwyżej dwa wiersze. W pierwszym wierszu będą opisane własności relacji oddzielone spacją. Do oznaczenia własności relacji należy używać skrótów, w następującej kolejności:

1. Z - zwrotność
2. PZ - przeciwzwrotność
3. S - symetria
4. AS - antysymetria
5. P - przechodniość
6. SP - spójność
7. X - żaden z powyższych

Jeżeli w pierwszym wierszu pojawiła się litera X, program kończy działanie. W przeciwnym przypadku, w wierszu drugim będzie opisany typ relacji. Do oznaczenia własności relacji należy używać oddzielonych spacjami skrótów, w następującej kolejności:

1. RR - relacja równoważności
2. RPL - relacja porządku liniowego
3. RPCz - relacja porządku częściowego – można użyć tylko wtedy, gdy nie było RPL
4. X - żaden z powyższych

## Przykłady

### Przykład 1

#### Wejście

```
2 3
2 9
3 4
5 7
5 9
6 7
7 8
8 3
```

#### Wyjście

```
PZ AS
X
```

## Opis rozwiązania 
W przypadku tego zadania praktycznie gotowy kod dla sprawdzenia wszystkich badanych własności jest dostępna na
**[Stronie uczelni](http://zeszyty-naukowe.wwsi.edu.pl/zeszyty/zeszyt3/O_Algorytmach_Badania_Wlasnosci_Relacji.pdf) (Kod funkcji od strony 6 PDF-a)**

Mając ten kod do dyspozycji jedynym, co pozostaje do zrobienia, są następujące zadania:
*(zakładam użycie C++14)* 
### Wczytywanie danych i ustawianie rozmiaru macierzy
Na początku musimy utworzyć tablicę 100x100 (maksymalny rozmiar):
```c++
int[100][100] macierz = {{}};
int n = 0; // Obecne maksimum 
```
Następnie wczytujemy dane po 2 na wiersz i aktualizujemy n (co ważne kod ten potrzebuje nagłówków `<iostream>` - wczytywanie i wypisywanie danych, `<algorithm>` - funkcja `max` dla skrócenia kodu)
```c++
int x,y;
while(cin>>x>>y) {
    n = max(n, max(x,y));
    //UWAGA: indeksy w zadaniu podawane są zaczynając od 1 a indeksy w tablicy zaczynają się od 0
    x -= 1;
    y -= 1;
    macierz[x][y] = 1;
}
```
### Wyznaczanie własności macierzy
Tu po prostu wywołujemy funkcje zaimplementowane na bazie **PDF rektora**.

### Wyznaczanie typu relacji

| Symbol | Typ relacji          | Warunek                                             |
|--------|----------------------|-----------------------------------------------------|
| RR     | Równoważności        | zwrotna && symetryczna && przechodnia               |
| RPL    | Porządku liniowego   | zwrotna && antysymetryczna && przechodnia && spojna |
| RPCz   | Porządku częściowego | zwrotna && antysymetryczna && przechodnia           |

**UWAGA:** Jeśli relacja **jest relacją porządku liniowego** to **NIE WYPISUJEMY PRCz**.

### Wypisywanie wyników
To jest dość proste, należy pamiętać jednak o kilku kwestiach:
- Linie mogą mieć dodatkowe spacje na końcu (to upraszcza wyświetlanie)
- **JEŚLI DLA WSZYSTKICH TYPÓW WŁASNOŚCI MAMY FAŁSZ NIE WYPISUJEMY TYPU RELACJI**
- **JEŚLI RELACJA JEST TYPU RPL TO NIE WYPISUJEMY DODATKOWO RPCz**
- **KOLEJNOŚĆ JEST WAŻNA** (podana pod koniec zadania)