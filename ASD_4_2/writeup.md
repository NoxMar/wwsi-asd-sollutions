# ASD_4_2 - Kolejka

## Treść zadania

Przed ponad dwunastu laty, w roku 2006 na portalu Spoj umieszczono zadanie STOS, którego celem miało być pokazanie sposobu działania stosu, poprzez jego zasymulowanie przy pomocy niewielkiej tablicy. Zadanie STOS miało walor dydaktyczny i dlatego zainspirowało nas do tego, aby podobny problem rozwiązać dla kolejki. Naszym celem będzie napisanie programu, który pozwoli zasymulować działanie kolejki, również przy pomocy niewielkiej tablicy.

Napisać program, który w 10-elementowej tablicy mogącej pomieścić liczby całkowite będzie symulował działanie kolejki. Kolejka na początku jest pusta. Potem, w zależności od pojawiających się na wejściu poleceń, ma się zapełniać lub opróżniać.

Język obsługujący kolejkę składa się z trzech komend. System poprawnie reaguje na każdą komendę, stosownie do jej treści, a następnie na konsoli pojawia się odpowiedni komunikat informujący o wykonanej czynności:

Enqueue x - komenda nakazująca umieszczenie liczby całkowitej x w kolejce. Po próbie jej wykonania, w osobnym wierszu pojawi się napis:
---> - informacja o poprawnym zapisie liczby x w kolejce;
Error: queue is full - informacja o tym, iż z powodu zapełnienia kolejki, liczba x nie może być w niej zapisana.
Dequeue - komenda nakazująca zdjęcie liczby z kolejki. Po próbie jej wykonania, w osobnym wierszu pojawi się napis:
x - wartość liczby zdjętej z kolejki;
Error: queue is empty - informacja o tym, iż kolejka jest pusta;
Print - komenda nakazująca wydrukowanie na konsoli zawartości kolejki w kolejności od głowy do jej ogona. Po próbie jej wykonania, w osobnym wierszu pojawi się napis:
Print: x1, x2, ..., xn - gdy kolejka nie jest pusta, na konsoli zostaną wydrukowane liczby całkowite tworzące zawartość kolejki;
Print: Queue is empty - napis informuje o tym, że kolejka jest pusta.

### Format wejścia

Na wejście programu podana zostanie pewna liczba poprawnych komend. Każda komenda zostanie zapisana w jednej linii.

### Format wyjścia

Na wyjściu programu w osobnych liniach pojawią się odpowiednie komunikaty informujące o skutkach wykonanych komend.
## Przykłady

### Przykład 1

#### Wejście

```
Enqueue 1
Enqueue 2
Enqueue 3
Enqueue 4
Enqueue 5
Print
Enqueue 6
Enqueue 7
Enqueue 8
Enqueue 9
Enqueue 10
Enqueue 11
Dequeue
Dequeue
Dequeue
Dequeue
Print
Dequeue
Dequeue
Dequeue
Dequeue
Dequeue
Print
Enqueue 12
Dequeue
Dequeue
Print
```

#### Wyjście

```
--->
--->
--->
--->
--->
Print: 1 2 3 4 5
--->
--->
--->
--->
--->
Error: queue is full
1
2
3
4
Print: 5 6 7 8 9 10
5
6
7
8
9
Print: 10
--->
10
12
Print: Queue is empty
```

## Opis rozwiązania

### Wczytywanie danych 

Jedyne, z czym nie mogliście się zetknąć to wczytywanie pojedynczych wyrazów. To najprościej wykonać używając strumienia `cin` i `string`:

```c++
string doWczytania;
cin >> doWczytania;
```
*Zakładamy obecność nagłówka `<string>` i `<iostream>` oraz użycie dyrektywy `using namespace std;`*

### Logika kolejki w implementacji na tablicy

Kolejka na tablicy jest **nieco bardziej złożona od stosu**, ponieważ mamy zarówno **licznik elementów** jak i **indeksu bazowego**. 

Wynika to z faktu, że w przypadku stosu **elementy były dodawane i zdejmowane z tej samej *strony* tablicy**. W przypadku kolejki natomiast elementy **usuwamy z początku** a **dodajemy na koniec** w wyniku czego **ilość elementów nie jest sama w sobie wystarczająca do wyznaczenia indeksów** co prezentuje poniższy przykład

(Element pierwszy następny do usunięcia w danym oznaczam **następująco** natomiast miejsce, w które zostanie dodany następny element *^w ten sposób^*)

| Operacja     | tab\[0\] | tab\[1\] | tab\[2\] | tab \[3\] | tab \[4\] | iloscElementow | indeksBazowy | Referencja |
|--------------|----------|----------|----------|-----------|-----------|----------------|--------------|------------|
| -            | **0**    | 0        | 0        | 0         | 0         | 0              | 0            | -          |
| Enqueue 1    | *^1^*    | **0**    | 0        | 0         | 0         | 1              | 0            | -          |
| Enqueue 2    | *^1^*    | 2        | **0**    | 0         | 0         | 2              | 0            | -          |
| Enqueue 3    | *^1^*    | 2        | 3        | **0**     | 0         | 3              | 0            | -          |
| Enqueue 4    | *^1^*    | 2        | 3        | 4         | **0**     | 4              | 0            | -          |
| Enqueue 5    | *^1^*    | 2        | 3        | 4         | 5         | 5              | 0            | -          |
| Dequeue => 1 | **0**    | *^2^*    | 3        | 4         | 5         | 4              | 1            | (1)        |
| Dequeue => 2 | **0**    | 0        | *^3^*    | 4         | 5         | 3              | 2            | -          |
| Enqueue 6    | 6        | **0**    | *^3^*    | 4         | 5         | 4              | 2            | (2)        |

**Ad (1)** zdejmujemy element, jak widać, z początku kolejki warto jednak zwrócić uwagę, że to powoduje, że następne miejsce w tablicy, na które możemy dodać, jest **NA POCZĄTKU TABLICY** pomimo bycia **końcem kolejki**.

**Ad (2)** gdy dodajemy do kolejki, która jest **"zawinięta"** dodajemy na w miejscu opisanym w *Ad(1)*.

### Opis operacji

Po analizie poprzedniego przykładu można **opisać kolejność kroków przy wykonaniu zadanych operacji** przy założeniu tak nazwanych zmiennych:

- `rozmiar_tablicy` - ile mamy miejsc w tablicy
- `ilosc_elementow`= 0 - obecna ilość elementów w tablicy
- `indesk_bazowy` = 0 - indeks do wstawienia kolejnego elementu
- `tablica` - tablica, na której bazuje kolejka

#### Enqueue `a`

1. Jeżeli `ilosc_elementow==rozmiar_tablicy` **zakończ działanie zgłaszający niemożliwość dodania**.
2. `tablica[(indesk_bazowy + ilosc_elementow) % rozmiar_tablicy] = a`
3. `ilosc_elementow += 1`
4. Zakończ procedure **zgłaszając poprawne dodanie**.

#### Dequeue

1. Jeżeli `ilosc_elementow == 0`  **zakończ działanie zgłaszający niemożliwość zdjęcia**.
2. **Zapisz do zwrócenia** wartość `tablica[indesk_bazowy]`
3. `ilosc_elementow -= 1`
4. `indesk_bazowy = (indesk_bazowy + 1) % rozmiar_tablicy`
5. **Zwróć zapisaną w kroku (2) wartość**.

#### Wypisz elementy

1. Jeżeli `ilosc_elementow == 0`  **zakończ działanie zgłaszający niemożliwość wypisania**.
2. W pętli dla i od `0`(*włącznie*) do `ilosc_elementow`(**wyłącznie**)
   1. Wypisz `tablica[(indesk_bazowy + i) % rozmiar_tablicy]`
   2. Wypisz ` `
3. Zakończ procedurę

### Uwagi do implementacji w języku C++

Aby **powiązać logicznie tablicę i liczniki** polecam użyć **struktury lub klasy**.