# Wypisywanie wyników

**ZAWSZE** wypisujemy **TYLKO I WYŁĄCZNIE TO, CZEGO OCZEKUJE SPECYFIKACJA ZADANIA**. Wynika to z faktu, że wyniki są sprawdzane poprzez **porównywanie znak do znaku** twojej **odpowiedzi** z **oczekiwaną** dla danego zestawu danych.

## Przykładowe zadanie

Aby pokazać, jak wypisywać dane sformułujmy trywialne zadanie i pokażmy jego rozwiązanie

### Polecenie

Wypisz liczby od `1` do `n` włącznie gdzie `n` jest podane na wejście. 

#### Przykład działania

##### Dane wejściowe
```
12
```
##### Wyjście
```
1
2
3
4
5
6
7
8
9
10
11
12
```

##### Kod rozwiązania

```c++
#include <iostream> //Dołączenie biblioteki udostępniającej operacje wejścia/wyjścia.

using namespace std; //Pozwala na ustawienie std jako przestrzeni domyślnej,
// aby nie musieć przed każdą funkcją biblioteczną pisać prefiksu `std::`

int main() {
    int n;
    cin >> n;//wczytujemy `n` ze strumienia wejściowego
    
    for (int i = 1; i <= n; i++) {
        cout << i << endl; //`cout` odpowiada za przekierowane danych do strumienia wyjściowego a `endl` dodaje znak 
  //nowej linii.
    }
    return 0; // w nowszych standardach można pominąć i w przypadku tego, co robimy, jest to pomijalne.
}
```