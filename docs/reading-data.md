# Wczytywanie danych

Zadania na platformie **SPOJ** mają kilka formatów, w których podawane są dane wejściowe.

## Nieznana ilość danych wejściowych, podany symbol kończący

Aby pokazać, jak wczytywać dane sformułujmy trywialne zadanie i pokażmy jego rozwiązanie

### Polecenie

Na wejście zostaje podany ciąg liczb całkowitych dodatnich po 1 na linię. Liczba `0` oznacza koniec ciąg.
Dla każdej liczby `n`, jeśli jest parzysta wypisujemy `2*n` a dla każdej nieparzystej `n+1`.

#### Przykład działania

##### Dane wejściowe
```
1
2
3
4
10
15
200
2345
0
```
##### Wyjście
```
2
4
4
8
20
16
400
2346
```

##### Kod rozwiązania

```c++
#include <iostream> //Dołączenie biblioteki udostępniającej operacje wejścia/wyjścia.

using namespace std; //Pozwala na ustawienie std jako przestrzeni domyślnej,
// aby nie musieć przed każdą funkcją biblioteczną pisać prefiksu `std::`

int main() {
    int n; // zmienna do przechowywania wartości wczytanej w danym kroku.
    
    while (true) {
        cin >> n; // wczytanie wartości ze strumienia wejściowego do zmiennej n
        if (n == 0) { // Sprawdzenie, czy element spełnia warunek bycia ostatnim elementem ciągu.
            break; // Jeśli tak wychodzimy z pętli.
        }
        cout << (n % 2 == 0 ? 2 * n : n + 1) << endl; // Wykonujemy opisane
// w treści zadania działanie zależnie od parzystości. Używam tu inline-if
// (ternary if). W dużym skrócie:
// <warunek> ? <zwróć jeśli warunek prawdziwy> : <zwróć jeśli warunek fałszywy>
    }
    return 0; // w nowszych standardach można pominąć i w przypadku tego, co robimy, jest to pomijalne.
}
```

###Nieznana liczba danych wejściowych, brak symbolu kończącego

W zadaniu z takim formatem wejściowym musimy skorzystać z faktu, że *operator `>>` strumienia `std::cin` zwraca **liczbę wczytanych wartości***. Z tego faktu można skorzystać **w warunku pętli**.

**UWAGA:** pętla tego typu, by zakończyć działanie, wymaga *zakończenia strumienia wejściowego* lub **wprowadzania danych, które nie mogą być zinterpretowane jako oczekiwany typ**. W wyniku tego najprostszym sposobem **wyjścia z pętli** czekającej na **wartości zwracanej przez `cin::>>`** jeśli próbujemy **wczytać liczbę** jest po prostu **wprowadzenie dowolnej litery**.


### Polecenie zadania demonstracyjnego

Na wejście zostaje podany ciąg liczb całkowitych z przedziału [-500, 500] po 1 na linię. Wypisz liczbę maksymalną.

#### Przykład działania

##### Dane wejściowe
```
1
2
3
-90
100
-234
456
-500
```
##### Wyjście
```
456
```

##### Kod rozwiązania

```c++
#include <iostream> //Dołączenie biblioteki udostępniającej operacje wejścia/wyjścia.

using namespace std; //Pozwala na ustawienie std jako przestrzeni domyślnej,
// aby nie musieć przed każdą funkcją biblioteczną pisać prefiksu `std::`

int main() {
    int n; // zmienna do przechowywania wartości wczytanej w danym kroku.
    int max = -501;
    //Jeśli udało się wczytać n to cin pozostanie w tgz "poprawnym stanie" co można sprawdzić metodą `.good()`. Alternatywną wersją tego warunku jest samo `cin>>n` w wyniku konwersji `cin`->`void*`->`bool` co jest krótsze, ale moim zdaniem mniej jasne.
    // while (cin >> n) { // Alternatywna wersja warunku
    while ((cin >> n).good()) {
        if (n > max) {
            max = n;
        }
    }
    cout << max << endl;
    return 0; // w nowszych standardach można pominąć i w przypadku tego, co robimy, jest to pomijalne.
}
```