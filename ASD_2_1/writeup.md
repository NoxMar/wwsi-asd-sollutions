# ASD_2_1 - Egocentryczny dyskretny ortogonalny świat N-oidów

## Treść zadania

Zapewne nie jest Ci obca opowieść o płaszczakach, płaskich istotach żyjących w przestrzeni dwuwymiarowej. Płaszczaki mogą się poruszać tylko po płaszczyźnie. W ich świecie, każdą zmianę położenia można opisać przy pomocy wektora o dwóch składowych.

W przeciwieństwie do płaszczaków, ludzie żyją w przestrzeni trójwymiarowej. Mogą wobec tego poruszać się w dowolnym kierunku w poziomie, a dzięki zdobyczom techniki także w pionie. W tym przypadku, zmianę położenia opisuje wektor o trzech składowych.

Wyobraź sobie, że gdzieś we wszechświecie można spotkać stworki, których przestrzeń ma n wymiarów. Na potrzeby naszej opowieści stworki te nazwiemy N-oidami. Świat N-oidów jest egocentryczny, dyskretny i ortogonalny:

- Egocentryczny, gdyż każdy z nich ma własny i dla niego jedyny kartezjański układ odniesienia, ze środkiem w jego własnym domu.
- Dyskretny, gdyż w tym świecie są znane tylko liczby całkowite.
- Ortogonalny, gdyż N-oidy poruszają się odcinkami po liniach prostych. Dodatkowo, dwa sąsiednie odcinki ich wędrówki są zawsze wzajemnie prostopadłe.

N-oid zanim wyjdzie z domu, w swoim notatniku ma zapisane kolejne odcinki swojej wędrówki w postaci ciągu wektorów o n całkowitych składowych. Wędrując, najpierw wyrusza w kierunku pierwszego wektora, a potem zmierza w kierunku kolejnych wektorów.

Dotychczas, w opisie wędrówki N-oidów nigdy nie było błędów: marszruta składała się z ciągu niezerowych wektorów, które w stosunku do wektora poprzedniego zawsze były prostopadłe. Niestety ostatnimi czasy sprawa się nieco skomplikowała. W opisie marszruty, z nieznanych przyczyn, zaczęły pojawiać się błędy. A to pojawia się wektor nie prostopadły do poprzednika, a to wektor o składowych zerowych.

Napisz program, który pomóże N-oidowi w jego wędrówce.

Dla każdego zestawu danych w opisie marszruty N-oida program powinien wyeliminować wektory zerowe lub nieprostopadłe do ostatniego prawidłowego wektora. Na końcu należy wypisać współrzędne punktu, do którego powinien dotrzeć N-oid wędrując zgodnie z prawidłowym opisem.

Przed przystąpieniem do napisania programu należy przyjąć następujące założenia:

- N-oid wystartował ze środka kartezjańskiego układu odniesienia
- Pierwszy wektor w marszrucie jest zdefiniowany prawidłowo

### Format wejścia

Pierwsza linijka wejścia zawiera dokładnie jedną liczbę całkowitą d ∈ `[1;4]`, określającą liczbę zestawów danych.

Każdy zestaw zaczyna się liczbą n ∈ `[2;10]` będącą rozmiarem przestrzeni, następnie w zestawie danych pojawia się liczba m ∈ `[2;500]` określająca liczbę wektorów w opisie marszruty. Następnie w m wierszach znajdują się współrzędne wektorów.
### Format wyjścia

W d liniach wyjścia należy podać dla każdego zestawu danych wektor określający położenie N-oida po zakończeniu wędrówki.

## Przykłady

### Przykład 1

#### Wejście

```
3
2
3
1 1
1 -1
0 2
4
5
1 2 3 1
0 2 -1 -1
1 0 1 -1
1 2 3 4
1 0 0 1
3
2
0 1 2
2 0 1
```

#### Wyjście

```
2 0 
3 6 6 3 
0 1 2
```

## Opis rozwiązania

Ponieważ większość trudności tego zadania pochodzi z dość *kreatywnego* opisu na początek przepiszę zadanie w sposób nieco czytelniejszy.

### Równoważna treść zadania

Opis pojedynczego problemu składa się z **listy wektorów** (tablic jednowymiarowych) o **takim samym rozmiarach**(`n`).

**Startując w punkcie `(0,0,...,0)`** wyznacz punkt, który będzie osiągnięty poprzez przejście ścieżką opisaną przez **wszystkie poprawne wektory** (alternatywnie można to opisać jako sumę po współrzędnych wszystkich poprawnych wektorów).
Wektor jest poprawny, jeżeli:
1) **Pierwszy** wektor **zawsze** jest **poprawny**
2) Wektor **nie** może być **zerowy**
3) Wektor musi być **prostopadły** do **ostatniego poprawnego wektora**.

- @2 - Wektor jest zerowy, jeśli **wszystkie jego składowe są zerowe** (wszystkie elementy tablicy są zerowe.
- @3 - Wektory są prostopadłe, jeśli **ich iloczyn skalarny** jest **równy 0**.  Dla przypomnienia iloczyn skalarny na 2 tablicach A i B obu o długości `n`:
```python
def iloczyn_skalarny(A, B, n):
    w = 0 # wynik
    for i in range(n):
        w += A[i] * B[i]
    return w
```
### Opis wejścia w języku przepisanego zadania
Pierwsza linia: **d** — liczba całkowita [1,4] opisująca **ilość problemów**.

Dla każdego problemu:

**n** — liczba całkowita [2,10] **wymiarów wektora** (elementów tablicy) w tym problemie 

**m** - liczba **wektorów w tym problemie**

**m linii po n elementów** - kolejne wektory po jednym na linie, elementy oddzielone spacjami


### Kod rozwiązania w języku `python`
**UWAGA**: Tak się **NIE PISZE PYTHON**-a, kod ten został napisany w ten sposób w celu największego **upodobnienia go do odpowiednika w C++**.
```python
def czy_niezerowy(wek, n):
    for i in range(n):
        if wek[i] != 0:
            return True
    return False


def czy_prostopadle(wek1, wek2, n):
    wynik = 0
    for i in range(n):
        wynik += wek1[i] * wek2[i]
    return wynik == 0


d = int(input())

for i in range(d):
    n = int(input())
    m = int(input())

    wczytany_wektor = [0] * 10
    ostatni_poprawny_wektor = [0] * 10
    wspolrzedne = [0] * 10

    for j in range(m):  # dla każdego z m wektorów
        wczytany_wektor = list(map(int, input().split(" ")))  # wczytywanie linii liczb całkowitych jako tablicy (w c++
        # pętla i cin>>)

        # sprawdzenie, czy wektor spełnia warunki
        if czy_niezerowy(wczytany_wektor, n) and czy_prostopadle(wczytany_wektor, ostatni_poprawny_wektor, n):
            for k in range(n):
                ostatni_poprawny_wektor[k] = wczytany_wektor[k]  # jeśli ten wektor byl poprawny to ustawiamy go jako
                # ostatni poprawny
                wspolrzedne[k] += wczytany_wektor[k]  # i dodajemy go do współrzędnych

    for j in range(n):
        print(wspolrzedne[j], end=" ")
    print()  # wypisuje nową linię na koniec wektora
```