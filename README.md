# Rozwiązania do zadań do wykładu z przedmiotu "Algorytmy i struktury danych" WWSI 2021/2022

## Uwagi wstępne

1) **Nie jestem specjalistą jak** chodzi o **C++**. Moimi głównymi językami programowania ją **Java** i **python3**.
2) **NIE POLECAM KOPIOWANIA ROZWIĄZAŃ** i umieszczania ich jako odpowiedzi w konkursie, ponieważ **REKTOR SPRAWDZA ROZWIĄZANIA PRZY UŻYCIU ANTYPLAGIATU**.
3) Z uwagi na punkt **2** do trudniejszych niż trywialne zadań będę umieszczał **rozwiązania w python3** napisane w taki sposób by jak **najprościej się przepisywało na C++** (bez użycia konstruktów "pytonicznych"). Dla zadań **całkowicie trywialnych** dostarczał będę tylko **schematy blokowe rozwiązania**.
4) Przykładowy kod w c++ jest pisany przy założeniu **C++14** i kompilatora **gcc**. Takie ustawienia polecam **wybrać na SPOJ**.

## Ogólnie przydatne do SPOJ-a

### [Wczytywanie danych](./docs/reading-data.md)

### [Wypisywanie danych](./docs/printing-results.md)

## Zadania i status wykonania

### Tydzień 1

#### [ASD_1_1 - Maksymalna liczba w ciągu](./a1/writeup.md)
| Status   | Sposób opisu rozwiązania    |
|----------|-----------------------------|
| Wykonane | Schemat blokowy rozwiązania | 

### Tydzień 2

### [ASD_1_2 - Minimalna liczba w ciągu](./ASD_1_2/writeup.md)
| Status   | Sposób opisu rozwiązania    |
|----------|-----------------------------|
| Wykonane | Schemat blokowy rozwiązania | 

### [FIBONUMS — Liczby Fibonacciego](./FIBONUMS/writeup.md)

| Status   | Sposób opisu rozwiązania            |
|----------|-------------------------------------|
| Wykonane | Opis algorytmu, kod w języku python |

### Tydzień 3

### [ASD_1_3 - Najmniejsza i największa liczba w ciągu](./ASD_1_3/writeup.md)
| Status   | Sposób opisu rozwiązania    |
|----------|-----------------------------|
| Wykonane | Schemat blokowy rozwiązania | 

### [ASD_1_4 -  Iloczyn macierzy](./ASD_1_4/writeup.md)

| Status   | Sposób opisu rozwiązania                                          |
|----------|-------------------------------------------------------------------|
| Wykonane | Opis, fragmenty kodu w języku python, fragmenty kodu w języku C++ |

### Tydzień 4

### [ASD_2_1 - Egocentryczny dyskretny ortogonalny świat N-oidów](./ASD_2_1/writeup.md)

| Status   | Sposób opisu rozwiązania                              |
|----------|-------------------------------------------------------|
| Wykonane | Uproszczone polecenie, opis, kod rozwiązania w python |

### [MWP3_1F1 - Relacja](./MWP3_1F1/writeup.md)
| Status   | Sposób opisu rozwiązania                                                         |
|----------|----------------------------------------------------------------------------------|
| Wykonane | Opis rozwiązania i uwagi, link do strony uczelni z implementacją badania relacji |

### Tydzień 5

### [ASD_4_1 - Mnożenie transpozycji macierzy przez wektor](./ASD_4_1/writeup.md)

| Status   | Sposób opisu rozwiązania                                          |
|----------|-------------------------------------------------------------------|
| Wykonane | Uwagi co do implementacji w C++, kluczowy fragment kodu w python3 |

### [ASD_5_1 - Sortowanie](./ASD_5_1/writeup.md)

| Status   | Sposób opisu rozwiązania                                   |
|----------|------------------------------------------------------------|
| Wykonane | Lista algorytmów do wyboru oraz sposobu wczytywania danych |

### [ASD_2_2 - Wycieczka](./ASD_2_2/writeup.md)

| Status   | Sposób opisu rozwiązania                                           |
|----------|--------------------------------------------------------------------|
| Wykonane | Uwagi i fragmenty co do implementacji w C++, pseudokod rozwiązania |

### Tydzień 6

### [ASD_5_3 - Nieziemska szkoła](./ASD_5_3/writeup.md)

| Status   | Sposób opisu rozwiązania                                        |
|----------|-----------------------------------------------------------------|
| Wykonane | Uwagi co do implementacji w C++, przydatne fragmenty kodu w C++ |

### [ASD_4_2 - Kolejka](./ASD_4_2/writeup.md)

| Status   | Sposób opisu rozwiązania                                                                         |
|----------|--------------------------------------------------------------------------------------------------|
| Wykonane | Dokładny opis kolejki. Lista kroków każdej z operacji na kolejce. Krótkie uwagi implementacyjne. |

### [ASD_6_1 - Składowe spójne grafu](./ASD_6_1/writeup.md)

| Status   | Sposób opisu rozwiązania                                                   |
|----------|----------------------------------------------------------------------------|
| Wykonane | Opis rozwiązania przy użyciu rozwiązań poprzedniego zadania i laboratoriów |
