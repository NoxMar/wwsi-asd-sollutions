# ASD_5_3 - Nieziemska szkoła

## Treść zadania

W pewnej nieziemskiej szkole zakończył się nabór uczniów do pierwszej klasy. Ponieważ nabór się przeciągnął, nie ma jeszcze dzienników, zaś nauczyciel w klasie ma do dyspozycji listę uczniów w takiej formie i w takiej kolejności, w jakiej uczniowi zostali wpisywani na listę w dniu naboru.

Szkoła jest nieziemska, to i problemy z listą są nieziemskie. Pierwszym problemem jest długość listy. Klasy nie mogą być zbyt liczne. Na liście, którą otrzymuje nauczyciel może być zapisanych nie więcej niż 50000 (słownie: pięćdziesiąt tysięcy) uczniów. Także zawartość listy jest dla nas nieco problematyczna. Na liście w kolejnych kolumnach znajdują się następujące elementy:

- Numer wskazujący na kolejność zapisu do szkoły;
- Numer ewidencyjny;
- Imię;
- Nazwisko.
Tutaj należy nadmienić, że rzeczywiste numery ewidencyjne w nieziemskiej rzeczywistości mogłyby zawierać dowolną ilość cyfr. Jednakże w obiegu urzędowym numery te są zaokrąglane, co najwyżej do sześciu cyfr po przecinku. Konsekwencją tego będzie fakt, iż w szkole mogą być uczniowie, którzy mimo różnego numeru ewidencyjnego, mogą mieć identyczne jego reprezentacje.

Ponieważ nauczyciele muszą uczniów oceniać i potem wpisywać im oceny, dlatego potrzebują mieć listę posortowaną w odpowiedniej kolejności. Twoim zadaniem będzie uporządkować listę, a następnie ją wydrukować.

Powinieneś napisać program, który niemalejąco posortuje dane w następującej kolejności:

- Numer ewidencyjny;
- Imię;
- Nazwisko.

### Format wejścia

NNa wejściu znajduje się nieznana liczba wierszy, w każdym wierszu znajdą się następujące elementy:

- Liczba całkowita dodatnia, określająca kolejność zapisu do szkoły;
- Nieziemski numer ewidencyjny (odpowiednik znanego nam numeru PESEL), będący mniejszą niż 10 rzeczywistą liczbą dodatnią.
- Zaczynające się dużą literą imię, zawierające nie więcej niż 5 znaków;
- Zaczynające się dużą literą nazwisko, zawierające nie więcej niż 10 znaków;

### Format wyjścia

Na wyjściu w każdym wierszu znajdą się oddzielone spacją następujące elementy:

- Numer wiersza w posortowanej liście;
- Nazwisko;
- Imię;
- Numer ewidencyjny;
- Numer określająca kolejność zapisu do szkoły.

## Przykłady

### Przykład 1

#### Wejście

```
1 0.455947 Bnqv Kwswmd
2 2.02185 Knzmv Ylxkuqv
3 1.26347 Qe Ysvroeqgpg
4 2.02185 I Ylxkuqv
5 0.73397 Wyo Tbhzkeopb
6 9.23338 E Sotp
7 2.02185 Knzmv Ylxkuqv
8 6.07776 Qpz Ujpqrzxmf
9 1.86132 Knzmv Ysegsjnnry
10 2.02185 Ocjg V
11 4.90555 Iaohb Ilqwsrfsaq
12 8.30897 Jp Ylxkuqv
13 2.02185 Knzmv Rxtevq
14 0.466933 M Ylxkuqv
15 4.32752 F Y
```

#### Wyjście

```
1 Ilqwsrfsaq Iaohb 4.90555 11
2 Kwswmd Bnqv 0.455947 1
3 Rxtevq Knzmv 2.02185 13
4 Sotp E 9.23338 6
5 Tbhzkeopb Wyo 0.73397 5
6 Ujpqrzxmf Qpz 6.07776 8
7 V Ocjg 2.02185 10
8 Y F 4.32752 15
9 Ylxkuqv I 2.02185 4
10 Ylxkuqv Jp 8.30897 12
11 Ylxkuqv Knzmv 2.02185 2
12 Ylxkuqv Knzmv 2.02185 7
13 Ylxkuqv M 0.466933 14
14 Ysegsjnnry Knzmv 1.86132 9
15 Ysvroeqgpg Qe 1.26347 3
```

## Opis rozwiązania

Zadanie to polega na zaimplementowaniu dowolnego sortowania **zalecam stabilnego** dla tablicy o nieznanym wcześniej rozmiarze. Samego algorytmu sortowania **nie mogę tu podać**, ponieważ algorytmów tych jest **sporo** i jak wszyscy **zaimplementują to samo** (w ten sam sposób), to będzie to **podejrzane**.

Dodatkowo implementacja ta musi **przyjmować własną funkcję porównania** i opracować **na wektorze lub tablicy typu złożonego np. klasy lub struktury**.

Wspomnę tylko, że algorytm prawdopodobnie powinien mieć **złożoność najgorszego przypadku `O(n long n)`** (lub lepszą). Takie algorytmy to np.:
- Merge sort (sortowanie przez scalanie)
- Timsort 

**Mergesort** jeśli dobrze pamiętam, więc najprościej będzie *pożyczyć* tamą implementacje z laboratoriów.

### Wczytywanie imienia i nazwiska

Aby wczytać imię, polecam użyć typu `string`. Przykład poniżej:
```c++
string imie;
cin >> imie;
```
**Uwaga** aby powyższy kod działał, niezbędne jest **załączenie nagłówków `<iostream>` i `<string>`**.


### Struktura

**Strukturę** w języku **C++** (we współczesnych standardach) możemy zdefiniować w następujący sposób:

```c++
struct student {
    int numerZapisania;
    double numerIdentyfikacyjny;
    string imie;
    //...Pozostałe pola
};
```

Następnie **poszczególne *instancje*** (to nie są instancje, ale to akceptowalna analogia z OOP) **tworzymy** w następujący sposób:
```c++
student adamAbacki{1234, 0.1, /*...*/};
```
Jednak do **przechowywania** i **przestawiania miejscami** **wygodniejsze i wydajniejsze** będą struktury allokowane dynamiczne:
```c++
student* adamAbacki = new student{1234, 0.1, /*...*/};
```
Wtedy jednak dostęp do pól mamy przez **operator wyłuskania `->`** np.: `adamAbacki->numerZapisania`.

### Sortowanie z własnym warunkiem 

Aby zaimplementować funkcję sortowania z własnym warunkiem, (w tym przypadku tablicę wskaźników na struktury) możemy zrobić to w następujący sposób:
```c++
void twojeMagiczneSortowanie(student** arr, int n, bool comparator(student* s1, student* s2)) {
    // Resztę sortowania musisz zaimplementować sam, żeby nie było potem na sesji "wojny klonów" :D ale pisałem, z kąd prawdopodobnie można wziąć.
    // w miejscu w algorytmie gdzie używałbyś dla liczb porównania `arr[i] <= arr[j]` używasz następującego warunku
    comparator(arr[i], arr[j]);
}
```


#### Wywołanie takiej funkcji sortowania 

*Następujące przykłady są dla porównania imion.*

Taką funkcję sortowania możemy wywołać przekazując komprator na jeden z dwóch sposobów:

##### Tworząc dodatkowe funkcje porównania

```c++
//Tworzenie komparatora

bool porownajImiona(student* s1, student* s2) {
    return s1 -> fn <= s2 -> fn;
}

//...
// Wywołanie
twojeMagiczneSortowanie(arr, n, porownajImiona)
```

##### Z anonimową funkcją porównania (lambdą), (tudzież funkcją, która spędza za dużo czasu na 4chan :D)

```c++
// Wywołanie
twojeMagiczneSortowanie(arr, n, [](student* s1, student* s2) {return s1 -> fn <= s2 -> fn;})
```

### Uwagi na temat porównań

Przy **nr ewidencyjnym** polecam **nie przejmować się uwagą o liczbie miejsc po przecinku**. Rozwiązanie zostanie uznane za poprawne, nawet jeśli nie będziecie się do niej stosować, a niepoprawne zaokrąglenie może prowadzić do odrzucenia odpowiedzi.

Dodatkowo **wystarczająco dobrym porównaniem liczb zmiennoprzecinkowych jest w tym przypadku**:
```c++
d1 <= d2
```
Jednak ogólnie dla liczb zmiennoprzecinkowych zaleca się **porównanie dopuszczające błąd** (z uwagi na błąd obliczeń zmiennoprzecinkowych) szczególnie **w przypadku *sprawdzania równości***. To uwaga **na przyszłość w tym zadaniu nie jest konieczne użycie porównania dopuszczającego błąd** zalecam porównanie **zaproponowane** przeze mnie **powyżej**.

### Rozwiązanie zadania przy użyciu powyższych fragmentów

Aby wykonać zadanie, trzeba kolejno wykonać następujące kroki:
1. Wczytanie listy (podobne jak w każdym zadaniu, w którym wczytywaliśmy z góry nieznaną liczbę elementów).
2. Posortowanie **po kolei** względem tych kryteriów
   - Numer ewidencyjny
   - Imię
   - Nazwisko
3. Wypisanie **zgodnie ze specyfikacją wyjścia zadania**.