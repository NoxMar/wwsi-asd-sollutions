# ASD_1_1 - Maksymalna liczba w ciągu

## Treść zadania

Dany jest ciąg nieujemnych liczb całkowitych zakończony zerem, przy czym zero nie jest elementem tego ciągu.

Należy znaleźć maksymalną liczbę w danym ciągu.

### Format wejścia
Na wejściu znajduje się ciąg nieujemnych liczb całkowitych, z przedziału **\[1;100\]**, zakończony zerem. **Zero** służy do określenia **końca ciągu**, w związku z czym wystąpi na wejściu **dokładnie raz**. Każdy element ciągu podany jest w **osobnej linii**.

### Format wyjścia
Na wyjściu należy wypisać wartość maksymalnej liczby w danym ciągu.

## Przykłady

### Przykład 1

#### Wejście

```
1
3
11
7
2
11
4
0
```

#### Wyjście

```
11
```
## Opis rozwiązania

Ponieważ rozwiązania jest **bardzo proste** umieszczę tylko **schemat blokowy rozwiązania**.

```mermaid
flowchart TD
    %%flow to exit
    start(Start)
    
    define-max[currentMax=-1]
    start --> define-max
    
    read-value[/wczytaj nextVal/]
    define-max --> read-value
    
    if-zero{nextVal==0}
    read-value --> if-zero
    
    print-current-max[/wypisz currentMax/]
    if-zero --> |TAK| print-current-max
    
    exit(Exit)
    print-current-max --> exit
    
    
    %% Flow through potentiol update
    if-next-val{nextVal > currentMax}
    if-zero --> |NIE| if-next-val
    
    
    %% Update currentMax if nextVal is greater
    update-max[currentMax=nextVal]
    if-next-val --> |TAK| update-max
    if-next-val --> |NIE| read-value
    %% Loop back to read
    update-max --> read-value
```
